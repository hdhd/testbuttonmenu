//
//  Image.swift
//  testButtonMenu
//
//  Created by NGUYEN HUU DANG on 2017/01/27.
//  Copyright © 2017 NGUYEN HUU DANG. All rights reserved.
//

import UIKit

extension UIImage {
    
    class func colorImage(color: UIColor, size: CGSize) -> UIImage? {
        UIGraphicsBeginImageContext(size)
        
        let rect = CGRect(origin: CGPoint.zero, size: size)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return image
}
}
