//
//  MenuButtonView.swift
//  testButtonMenu
//
//  Created by NGUYEN HUU DANG on 2017/01/27.
//  Copyright © 2017 NGUYEN HUU DANG. All rights reserved.
//

import UIKit

class MenuButtonView: UIView {

    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    private let defaultTopMargin: CGFloat = 4
    private var menuObject: HomeMenuObject!

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setup()
    }
    
    func setup() {
        self.setupAutoLayout()
        
        self.button.isExclusiveTouch = true
    }
    
    
    private func setupAutoLayout() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "MenuButtonView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        let bindings = ["view": view]
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|",
                                                      options: NSLayoutFormatOptions(rawValue: 0),
                                                      metrics: nil,
                                                      views: bindings))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|",
                                                      options: NSLayoutFormatOptions(rawValue: 0),
                                                      metrics: nil,
                                                      views: bindings))
    }
    
    func setup(menuObject: HomeMenuObject) {
        self.menuObject = menuObject
        self.button.layer.borderWidth = 1
        self.button.layer.borderColor = menuObject.color.cgColor
        self.button.layer.cornerRadius = 2
        self.button.tag = menuObject.tag
        self.button.setTitle(menuObject.title, for: .normal)
        
        selected(isSelected: false)
    }
    
    
    func selected(isSelected: Bool) {
        self.button.isSelected = isSelected
        
        if isSelected {
            self.button.setTitleColor(UIColor.white, for: .normal)
            self.button.setBackgroundImage(UIImage.colorImage(color: self.menuObject.color, size: self.button.frame.size), for: .normal)
            self.topConstraint.constant = 0
            
        } else {
            self.button.setBackgroundImage(UIImage.colorImage(color: UIColor.white, size: self.button.frame.size), for: .normal)
            self.topConstraint.constant = defaultTopMargin
        }
    }
}
