//
//  HomeMenuObject.swift
//  testButtonMenu
//
//  Created by NGUYEN HUU DANG on 2017/01/27.
//  Copyright © 2017 NGUYEN HUU DANG. All rights reserved.
//

import UIKit

class HomeMenuObject {
    
    enum MenuSection {
        case myPage
        case recommend
        case event
        case program
    }
    
    var menuSection: MenuSection
    var title: String
    var tag: Int
    var color: UIColor
    
    init(menuSection: MenuSection) {
        self.menuSection = menuSection
        
        switch menuSection {
        case .myPage:
            self.title = "マイページ"
            self.tag = 0
            self.color = UIColor.purple
        case .recommend:
            self.title = "おすすめ"
            self.tag = 1
            self.color = UIColor.blue
        case .event:
            self.title = "イベント"
            self.tag = 2
            self.color = UIColor.brown
        case .program:
            self.title = "番組"
            self.tag = 3
            self.color = UIColor.yellow
        }
    }
    
}
