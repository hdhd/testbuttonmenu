//
//  ViewController.swift
//  testButtonMenu
//
//  Created by NGUYEN HUU DANG on 2017/01/27.
//  Copyright © 2017 NGUYEN HUU DANG. All rights reserved.
//

import UIKit
import PagingMenuController
import Cartography

class ViewController: UIViewController {

    private struct PagingMenuOptions: PagingMenuControllerCustomizable {
        fileprivate var componentType: ComponentType {
            return .pagingController(pagingControllers: pagingControllers)
        }
        
        fileprivate var animationDuration: TimeInterval {
            return 0.0
        }
        
        fileprivate var pagingControllers: [UIViewController] {
            let storyboardName = "Main"
            let viewController1 = TestViewController.instantiate(storyboard: storyboardName)
            let viewController2 = TestViewController.instantiate(storyboard: storyboardName)
            let viewController3 = TestViewController.instantiate(storyboard: storyboardName)
            let viewController4 = TestViewController.instantiate(storyboard: storyboardName)
            
            return [viewController1, viewController2, viewController3, viewController4]
        }
    }
    private var menuBorderView: UIView!
    private let menuBoderHeigt: CGFloat = 1
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var contentView: UIView!
    
    fileprivate var pagingMenuController: PagingMenuController!
    fileprivate var page = 0
    private var menuObjects = [HomeMenuObject]()
    private var menuButtonViews = [MenuButtonView]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setup() {
        setupMenu()
        setupPageViewController()
        
    }
    
    func pressMenuButton(button: UIButton) {
        self.move(toPage: button.tag)
    }
    
    
    fileprivate func changeMenuButtonSelectedState() {
        for menuButtonView in self.menuButtonViews {
            let selected = menuButtonView.button.tag == self.page
            menuButtonView.selected(isSelected: selected)
        }
        
        UIView.animate(
            withDuration: 0.3
            ,delay: 0.0
            ,options: .curveEaseOut
            ,animations: {() -> Void in
                self.menuView.layoutIfNeeded()
        }
            ,completion: nil
        );
        
        let menuObject = self.menuObjects[self.page]
        self.menuBorderView.backgroundColor = menuObject.color
    }

    
    
    private func setupMenu() {
        self.menuView.subviews.forEach {$0.removeFromSuperview()}
        self.menuButtonViews.removeAll()
        self.menuObjects.removeAll()
        
        let menuObject1 = HomeMenuObject(menuSection: .myPage)
        let menuObject2 = HomeMenuObject(menuSection: .recommend)
        let menuObject3 = HomeMenuObject(menuSection: .event)
        let menuObject4 = HomeMenuObject(menuSection: .program)
        self.menuObjects.append(contentsOf: [menuObject1, menuObject2, menuObject3, menuObject4])
        
        var lastButtonView: MenuButtonView?
        
        for i in 0..<self.menuObjects.count {
            let weekObject = menuObjects[i]
            let buttonView: MenuButtonView = {
                $0.setup(menuObject: weekObject)
                $0.button.addTarget(self, action: #selector(pressMenuButton(button:)), for: .touchUpInside)
                return $0
            }(MenuButtonView(frame: CGRect.zero))
            
            self.menuView.addSubview(buttonView)
            self.menuButtonViews.append(buttonView)
            if let lastButtonView = lastButtonView {
                constrain(buttonView, lastButtonView) { view1, view2 in
                    view1.top == view1.superview!.top
                    view1.left == view2.right
                    view1.bottom == view1.superview!.bottom
                    view1.width == view1.superview!.width / CGFloat(self.menuObjects.count)
                }
            } else {
                constrain(buttonView) { view in
                    view.top == view.superview!.top
                    view.left == view.superview!.left
                    view.bottom == view.superview!.bottom
                    view.width == view.superview!.width / CGFloat(self.menuObjects.count)
                }
            }
            
            lastButtonView = buttonView
        }
        self.setupmenuBorder()
        let selectedbutton = menuButtonViews[self.page]
        selectedbutton.selected(isSelected: true)
    }
    
    private func setupmenuBorder(){
        let boderView: UIView = {
            $0.backgroundColor = self.menuObjects[self.page].color
            self.menuView.addSubview($0)
            return $0
        }(UIView(frame: CGRect.zero))
        
        constrain(boderView){ view in
            view.left == view.superview!.left
            view.bottom == view.superview!.bottom
            view.right == view.superview!.right
            view.height == self.menuBoderHeigt
        }
        
        self.menuBorderView = boderView
    }
    
    private func setupPageViewController() {
        let options = PagingMenuOptions()
        let pagingMenuController = PagingMenuController(options: options)
        pagingMenuController.delegate = self
        self.pagingMenuController = pagingMenuController
        
        addChildViewController(pagingMenuController)
        self.contentView.addSubview(pagingMenuController.view)
        
        constrain(pagingMenuController.view, self.contentView) { view1, view2 in
            view1.top == view2.top
            view1.left == view2.left
            view1.right == view2.right
            view1.bottom == view2.bottom
        }
        
        pagingMenuController.didMove(toParentViewController: self)
    }
    
    
    private func move(toPage: Int) {
        self.page = toPage
        
        self.pagingMenuController.move(toPage: toPage, animated: false)
        
        self.changeMenuButtonSelectedState()
    }



}


extension ViewController: PagingMenuControllerDelegate {
    
    func willMove(toMenu menuController: UIViewController, fromMenu previousMenuController: UIViewController) {
    }
    
    func didMove(toMenu menuController: UIViewController, fromMenu previousMenuController: UIViewController) {

        
        self.page = self.pagingMenuController.currentPage
        self.changeMenuButtonSelectedState()
    }
    
}

